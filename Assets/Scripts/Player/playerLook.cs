
using UnityEngine;

public class playerLook : MonoBehaviour
{
    [SerializeField] private Transform playerBody;

    [Header("Ustawienia myszki")]
    public float mouseSensitivity = 100f;

    private float xRotation;

    private Vector2 mouseVector;

    private InputMaster controls;

    void Start()
    {
        //Ustawiamy Input
        controls = new InputMaster();
        controls.Enable();

        controls.Player.Mouse.performed += ctx => mouseVector = ctx.ReadValue<Vector2>();

        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        mouseVector.x = mouseVector.x * mouseSensitivity * Time.deltaTime;
        mouseVector.y = mouseVector.y * mouseSensitivity * Time.deltaTime;

        // Clampujemy kamer�
        xRotation -= mouseVector.y;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        // Obracamy cia�o gracza
        transform.localRotation = Quaternion.Euler(xRotation, 0, 0);
        playerBody.Rotate(Vector3.up * mouseVector.x);

    }
}
