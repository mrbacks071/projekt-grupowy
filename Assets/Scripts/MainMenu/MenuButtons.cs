using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButtons : MonoBehaviour
{
    public void NewGameBtn() {
        SceneManager.LoadSceneAsync(1);
    }

    public void ExitBtn() {
        Application.Quit();
    }
}
