using UnityEngine;

public class DebugUI : MonoBehaviour {
    private CommandLine commandLine;
    private OutputDebug outputDebug;
    private DebugUIKeymap keymap;

    private void Awake() {
        commandLine = GetComponentInChildren<CommandLine>();
        outputDebug = GetComponentInChildren<OutputDebug>();
        commandLine.Enabled = false;
        outputDebug.Enabled = false;

        keymap = new DebugUIKeymap();
        keymap.DebugUI.Enable();
        keymap.DebugUI.ToggleDebugUI.performed += ToggleCommandLine_performed;
        keymap.DebugUI.SendCommand.performed += SendCommand_performed;
        keymap.DebugUI.MemoryCommandUp.performed += MemoryCommandUp_performed;
        keymap.DebugUI.MemoryCommandDown.performed += MemoryCommandDown_performed;
    }

    private void ToggleCommandLine_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj) {
        commandLine.Enabled = !commandLine.Enabled;
        outputDebug.Enabled = commandLine.Enabled;
    }

    private void SendCommand_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj) {
        commandLine.SendCommand();
    }

    private void MemoryCommandUp_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj) {
        commandLine.MemoryCommandUp();
    }

    private void MemoryCommandDown_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj) {
        commandLine.MemoryCommandDown();
    }

    private void OnDisable() {
        if (keymap != null && keymap.DebugUI.enabled) {
            keymap.Disable();
        }
    }
}
