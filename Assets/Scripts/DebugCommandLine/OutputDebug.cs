using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OutputDebug : MonoBehaviour {
    public TextMeshProUGUI debugText;
    public float refreshRateInSeconds;
    public int logMemorySize;
    public Color textDefault;
    public Color textLog;
    public Color textWarning;
    public Color textError;
    public Color textException;

    public Scrollbar dialogScrollbar;

    private bool firstRunFix;
    private bool isEnabled;
    private float refreshTimer;
    private Queue<string> memory;

    private bool isMessageToShow;

    private void Awake() {
        memory = new Queue<string>();
        firstRunFix = false;
        refreshTimer = refreshRateInSeconds;
    }


    private void Start() {
        ClearDebugLog();
        Application.logMessageReceived += HandleLog;
    }

    private void Update() {
        if (isEnabled && refreshTimer < refreshRateInSeconds) {
            refreshTimer += Time.unscaledDeltaTime;
        }

        if (isMessageToShow)
            RefreshTextField();
    }

    private void OnEnable() {
        isEnabled = true;
        if (firstRunFix)
            RefreshTextField();
        else
            firstRunFix = true;
    }

    private void OnDisable() {
        isEnabled = false;
        refreshTimer = refreshRateInSeconds;
    }

    public void ClearDebugLog() {
        memory.Clear();
        debugText.text = "";
    }

    public void DevDebugLog(string text, LogType type) {
        isMessageToShow = true;

        Color messageColor = textDefault;
        if (type == LogType.Log) messageColor = textLog;
        if (type == LogType.Warning) messageColor = textWarning;
        if (type == LogType.Error) messageColor = textError;
        if (type == LogType.Exception) messageColor = textException;

        memory.Enqueue($"<color=#{ColorUtility.ToHtmlStringRGB(messageColor)}>[{GameUtils.EnumValueName(type)}] {text}");
        if (memory.Count > logMemorySize) {
            memory.Dequeue();
        }
    }

    private void RefreshTextField() {
        if (isEnabled && refreshTimer >= refreshRateInSeconds) {
            debugText.text = string.Join('\n', memory);
            refreshTimer = 0;
            StartCoroutine(SetScrollbarToBottom());
            isMessageToShow = false;
        }
    }

    IEnumerator SetScrollbarToBottom() {
        yield return new WaitForEndOfFrame();
        dialogScrollbar.value = 0;
    }

    void HandleLog(string logString, string stackTrace, LogType type) {
        DevDebugLog(logString, type);
    }

    public bool Enabled {
        get { return gameObject.activeSelf; }
        set {
            gameObject.SetActive(value);
        }
    }
}