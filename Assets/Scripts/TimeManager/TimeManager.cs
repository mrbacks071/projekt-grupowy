using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour 
{
   

    private float initialFixedTimestep;

    private void Awake()
    {
        initialFixedTimestep = Time.fixedDeltaTime;
    }


    public bool isTimeStill()
    {
        return Time.timeScale == 0f;
    }

    public void ToggleFreezeTime()
    {
        Time.timeScale = Time.timeScale != 0f ? 0f : 1f;
        Time.fixedDeltaTime = initialFixedTimestep * Time.timeScale;
    }

    public void SpeedUpTimeBy(float speedUpTimeStep)
    {
        Time.timeScale += speedUpTimeStep;
        Time.fixedDeltaTime = Time.fixedDeltaTime * Time.timeScale;
    }

    public void SlowDownTimeBy(float slowDownTimeStep)
    {
        Time.timeScale -= slowDownTimeStep;
        Time.fixedDeltaTime = Time.fixedDeltaTime * Time.timeScale;
    }

    public void SetTimescaleTo(float slowDownTimeTo)
    {
        Time.timeScale = slowDownTimeTo;
        Time.fixedDeltaTime = initialFixedTimestep * Time.timeScale;
    }
    
    public void ResetTimeScaleToDefault()
    {
        Time.timeScale = 1f;
        Time.fixedDeltaTime = initialFixedTimestep;
    }

}
